<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::post('register', 'Api\UserController@register');
Route::post('login', 'Api\UserController@authenticate');

Route::group(['middleware' => ['jwt']], function() {
	Route::get('refresh', 'Api\UserController@refresh');
	Route::get('logout', 'Api\UserController@logoutUser');
	Route::get('user', 'Api\UserController@getAuthenticatedUser');
	Route::get('profile', 'Api\UserController@getUserProfile');
	Route::get('categories', 'Api\CategoryController@getCategoryLists');
	Route::post('category-items', 'Api\CategoryController@getCategoryItemLists');
	Route::get('latest-items', 'Api\ItemController@getLatestItems');
	Route::post('addOrder', 'Api\OrderController@addItemToOrder');
	Route::post('addToCart', 'Api\OrderController@addItemToCart');
	Route::post('deleteFromCart', 'Api\OrderController@deleteFromCart');
	Route::get('carts', 'Api\OrderController@getListOfItemsInCart');
	Route::post('order', 'Api\OrderController@addToOrder');
	Route::get('cart-count', 'Api\OrderController@getCartTotalCount');
});
Route::get('items/categories/{id}', 'Api\CategoryController@getItemLists');
Route::post('packagesByCategory', 'Api\PackageController@getPackageListsByCategory');