<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/portal/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::resource('category', 'Admin\CategoryController');
Route::resource('item', 'Admin\ItemController');
Route::resource('user', 'Admin\UserController');
Route::get('get-item-size-price/{id}', 'Admin\ItemController@loadItemExtraPrice')->name('loadItemExtraPrice');
Route::post('item-size-price', 'Admin\ItemController@addExtraPriceItem')->name('addExtraPriceItem');
