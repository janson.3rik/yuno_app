@extends('admin.layouts.master')

@section('content')
<h1 class="h3 mb-4 text-gray-800">{{ $data['panel'] }}</h1>
<div class="row">
    <div class="col-4">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseForm" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseForm">
              <h6 class="m-0 font-weight-bold text-primary">Add Category</h6>
            </a>

            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseForm">
              <div class="card-body">
                <form id="categoryForm" method="post" action="javascript:void(0)">
                  @csrf
                    <div class="form-group">
                        <label for="">Choose Parent Category</label>
                        <select name="parentCategory" class="form-control" id="parentCategory">
                            <option value="">Please Choose</option>
                        </select>
                        <span class="text-danger">{{ $errors->first('parentCategory') }}</span>
                    </div>

                    <div class="form-group">
                        <label for="">Category Title</label>
                        <input type="text" class="form-control" id="categoryTitle" placeholder="Enter Category Title">
                        <span class="text-danger">{{ $errors->first('categoryTitle') }}</span>
                    </div>

                    <div class="form-group">
                        <label for="">Category Title</label>
                        <input id="fileupload" type="file" name="file" data-folder="category">
                        <span class="text-danger">{{ $errors->first('file') }}</span>
                    </div>

                    <div class="form-group">
                        <label for="">Category Title</label>
                        <textarea class="form-control" id="summernote" name="description">
                        </textarea>
                        <span class="text-danger">{{ $errors->first('description') }}</span>

                    </div>

                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" class="form-control" id="status" >
                            @foreach (json_decode('{"1": "Published", "0": "Not Published"}', true) as $optionKey => $optionValue)
                                <option value="{{ $optionKey }}" {{ (isset($data['row']->status) && $data['row']->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('status') }}</span>

                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
              </div>
            </div>
        </div>
    </div>

    <div class="col-8">
        <div class="card shadow mb-4">
            
            <!-- Card Header - Accordion -->
            <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
              <h6 class="m-0 font-weight-bold text-primary">Collapsable Card Example</h6>
            </a>

            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseCardExample">
              <div class="card-body">
                This is a collapsable card example using Bootstrap's built in collapse functionality. <strong>Click on the card header</strong> to see the card body collapse and expand!
              </div>
            </div>

        </div>
    </div>
</div>

@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/vendor/summernote/summernote-bs4.css') }}">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/backend/vendor/summernote/summernote-bs4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 150,
      });
    });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
@endsection