@extends('admin.layouts.master')

@section('content')
<h1 class="h3 mb-4 text-gray-800">{{ $data['panel'] }}</h1>
<div class="row">
    <div class="col-8">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseForm" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseForm">
              <h6 class="m-0 font-weight-bold text-primary">Add Category</h6>
            </a>

            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseForm">
              <div class="card-body">
                <form id="categoryForm" method="post" action="{{ route('category.store') }}" enctype="multipart/form-data">
                    @csrf
                    @include('admin.category.form')
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/vendor/summernote/summernote-bs4.css') }}">
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/backend/vendor/summernote/summernote-bs4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 150,
      });
    });
</script>
@endsection