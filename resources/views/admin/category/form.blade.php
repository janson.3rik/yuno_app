<div class="form-group">
    <label for="">Choose Parent Category</label>
    <select name="parentCategory" class="form-control" id="parentCategory">
        <option value="">Please Choose</option>
            @foreach (json_decode($data["categories"], true) as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($data['row']->parent_id) && $data['row']->parent_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
    </select>
    <span class="text-danger">{{ $errors->first('parentCategory') }}</span>
</div>

<div class="form-group">
    <label for="">Category Title</label>
    <input type="text" class="form-control" id="categoryTitle" name="categoryTitle" placeholder="Enter Category Title" autocomplete="off" value="{{ old('categoryTitle')?? (isset($data['row'])? $data['row']->title:'')  }}">
    <span class="text-danger">{{ $errors->first('categoryTitle') }}</span>
</div>

<div class="form-group">
    <label for="">Category Featured Image</label>
    <input id="fileupload" type="file" name="file" data-folder="category">
    <span class="text-danger">{{ $errors->first('file') }}</span>
</div>

<div class="form-group">
    <label for="">Category Title</label>
    <textarea class="form-control" id="summernote" name="description" autocomplete="off">
        {{ old('description')?? (isset($data['row'])? $data['row']->content:'') }}
    </textarea>
    <span class="text-danger">{{ $errors->first('description') }}</span>

</div>

<div class="form-group">
    <label for="">Status</label>
    <select name="status" class="form-control" id="status" >
        @foreach (json_decode('{"1": "Published", "0": "Not Published"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($data['row']->status) && $data['row']->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
    <span class="text-danger">{{ $errors->first('status') }}</span>

</div>