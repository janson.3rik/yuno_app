@extends('admin.layouts.master')

@section('content')
<h1 class="h3 mb-4 text-gray-800">{{ $data['panel'] }}</h1>
<div class="row">
    <div class="col-8">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseForm" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseForm">
              <h6 class="m-0 font-weight-bold text-primary">Edit Category</h6>
            </a>

            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseForm">
              <div class="card-body">
                <form method="POST" action="{{ route('item.update', ['id' => $data['row']->id]) }}" accept-charset="UTF-8" enctype="multipart/form-data" id="editForm">
                    <input type="hidden" name="_method" value="PUT">
                    @csrf
                    @include('admin.item.form')
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
              </div>
            </div>
        </div>
    </div>
    <div class="col-4">
        @if(isset($data['row']))
            <img src="{{ $data['row']->processImage()}}" width="100%" height="250px">
        @endif
    </div>
</div>

@endsection
