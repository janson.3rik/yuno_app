@extends('admin.layouts.master')

@section('content')
<h1 class="h3 mb-4 text-gray-800">{{ $data['panel'] }}</h1>
<div class="row">
    <div class="offset-2 col-8 offset-2">
        <div class="card shadow mb-4">
            <!-- Card Header - Accordion -->
            <a href="#collapseForm" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseForm">
              <h6 class="m-0 font-weight-bold text-primary">Add Item</h6>
            </a>

            <!-- Card Content - Collapse -->
            <div class="collapse show" id="collapseForm">
              <div class="card-body">
                <form id="categoryForm" method="post" action="{{ route('item.store') }}" enctype="multipart/form-data">
                    @csrf
                    @include('admin.item.form')
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
              </div>
            </div>
        </div>
    </div>
</div>

@endsection
