<div class="form-group">
    <label for="">Choose Category</label>
    <select name="category_id" class="form-control" id="parentCategory">
        <option value="">Select Category</option>
            @foreach (json_decode($data["categories"], true) as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($data['row']->category_id) && $data['row']->category_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
    </select>
    <span class="text-danger">{{ $errors->first('category_id') }}</span>
</div>

<div class="form-group">
    <label for="">Name</label>
    <input type="text" class="form-control" id="itemName" name="title" placeholder="Enter Item Name" autocomplete="off" value="{{ old('title')?? (isset($data['row'])? $data['row']->title:'')  }}">
    <span class="text-danger">{{ $errors->first('title') }}</span>
</div>

<div class="form-group">
    <label for="">Featured Image</label>
    <input id="fileupload" type="file" name="file" data-folder="items">
    <span class="text-danger">{{ $errors->first('file') }}</span>
</div>

<div class="form-group">
    <label for="">Description</label>
    <textarea class="form-control" id="summernote" name="description" autocomplete="off">
        {{ old('description')?? (isset($data['row'])? $data['row']->description:'') }}
    </textarea>
    <span class="text-danger">{{ $errors->first('description') }}</span>
</div>


<div class="form-group">
    <label for="">Has Different Price?</label>
    <select name="hasSize" class="form-control" id="hasSize" >
        @foreach (json_decode('{"0": "No", "1": "Yes"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($data['row']->hasSize) && $data['row']->hasSize == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
    <span class="text-danger">{{ $errors->first('status') }}</span>
</div>

<div class="form-group" id="priceForm">
    <label for="">Price</label>
    <input type="text" class="form-control" id="itemPrice" name="price" placeholder="Enter Item Name" autocomplete="off" value="{{ old('price')?? (isset($data['row'])? $data['row']->price:'')  }}">
    <span class="text-danger">{{ $errors->first('price') }}</span>
</div>
@if(isset($data['row']))
<div class="form-group" id="showExtraPriceForm">
@include('admin.item.subform')
</div>
@endif

<div class="form-group">
    <label for="" class="mr-2">Item Type</label>
    @php
        if(isset($data['row'])):
            $notApplicable = ($data['row']->dishType == 'NotApplicable')? 'checked':'';
            $veg = ($data['row']->dishType == 'Veg')? 'checked':'';
            $nonVeg = ($data['row']->dishType == 'NonVeg')? 'checked':'';
        endif    
    @endphp
    <input type="radio" name="dishType" value="NotApplicable" {{( $notApplicable)?? '' }} > <span class="text-gray-700 mr-1 font-weight-bold"> Not Applicable </span>
    <input type="radio" name="dishType" value="Veg" {{ ($veg)?? ''}}> <span class="text-success mr-1 font-weight-bold"> Vegeterian</span> 
    <input type="radio" name="dishType" value="NonVeg" {{ ($nonVeg)?? ''}}> <span class="text-danger mr-1 font-weight-bold"> Non Vegeterian</span>
    <span class="text-danger">{{ $errors->first('dishType') }}</span>
</div>

<div class="form-group row">
    <div class="col-6">
         <label for="">Featured</label>
        <select name="featured" class="form-control" id="featured" >
            @foreach (json_decode('{"1": "Yes", "0": "No"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($data['row']->featured) && $data['row']->featured == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        <span class="text-danger">{{ $errors->first('featured') }}</span>
    </div>
    <div class="col-6">
        <label for="">Status</label>
        <select name="status" class="form-control" id="status" >
            @foreach (json_decode('{"1": "Published", "0": "Not Published"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($data['row']->status) && $data['row']->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        <span class="text-danger">{{ $errors->first('status') }}</span>
    </div>
</div>

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/vendor/summernote/summernote-bs4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/backend/vendor/select2/css/select2.min.css') }}">

@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/backend/vendor/summernote/summernote-bs4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/backend/vendor/select2/js/select2.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        
        function showHideExtraPriceForm(hasSize){
            var formId = hasSize.parents("form").attr("id");

            if( formId == 'editForm' && hasSize.val() == 1){
                    $('#showExtraPriceForm').show();
            }else if(formId == 'editForm' && hasSize.val() == 0){
                $('#showExtraPriceForm').hide();
            }else if(hasSize.val() == 0){
                $('#showExtraPriceForm').hide();
            }
        }

        function checkForPrice(){
            if($('#hasSize').val() == 1){
                $('#priceForm').hide();
                showHideExtraPriceForm($('#hasSize'));
               
            }else{
                $('#priceForm').show();
                showHideExtraPriceForm($('#hasSize'));
            }
        }
      
        checkForPrice();      
        $("#parentCategory").select2({
            placeholder: "Select a Category",
            allowClear: true
        });

        $('#summernote').summernote({
            height: 150,
        });
        $('#hasSize').on('change',function(e){
            checkForPrice();
        });


    });
</script>
@endsection
