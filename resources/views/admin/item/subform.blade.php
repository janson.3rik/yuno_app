 <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#collapseSubForm" class="d-block card-header bg-success text-white" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseSubForm">
          <h6 class="m-0 font-weight-bold text-white">Add Extra Price</h6>
        </a>

	  	<!-- Card Content - Collapse -->
		<div class="collapse show" id="collapseSubForm">
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<a href="javascript:;" class="btn btn-primary mb-2" onclick="showModal();">Add Price</a>
					</div>
					<br>	
					<div class="col-12">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>Size</th>
									<th>Price</th>
								</tr>
							</thead>
							<tbody id="itemExtraTable">

							</tbody>
						</table>
					</div>
				</div>
				<table>

				</table>
			</div>	
		</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Extra Price</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	
      	Select Size Price for {{ $data['row']->title }}
      	<input type="hidden" name="itemId" id="itemId" value="{{ $data['row']->id }}">
      	<div class="form-group row">
  			<div id="alert-message" class="alert"></div>
      		<div class="col-6">
      			<select class="form-control" name="extraPriceSize" id="extraPriceSize">
      				<option value="">Please Select Price</option>
      				<option value="small">Small</option>
      				<option value="medium">Medium</option>
      				<option value="large">Large</option>
      				<option value="default">Default</option>
      			</select>
      		</div>
      		<div class="col-6">
      			<input type="text" name="extraPrice" id="extraPrice" class="form-control">
      		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="addExtraPrice">Save changes</button>
      </div>
    </div>
  </div>
</div>



@section('scripts')
    @parent
    <script type="text/javascript">
    	function loadData(){
    			$.ajax({
		           type:'GET',
		           url:'{{ route('loadItemExtraPrice',$data['row']->id) }}',
		           success:function(data, textStatus, xhr){
		           	if(xhr.status == 200){
		           		$('#itemExtraTable').empty();
		           		$('#itemExtraTable').html(data);
		           	}else{
		           		message = jQuery.parseJSON( data.responseText );
		           		$('#itemExtraTable').empty();
				        $('#itemExtraTable').html(message.msg);
		           	}
		           },
		           error: function (data, textStatus, errorThrown) {
				        message = jQuery.parseJSON( data.responseText );
				        $('#itemExtraTable').empty();
				        $('#itemExtraTable').html(message.msg);
				   }
		        });

    		}

		loadData();
		console.log($('#hasSize').val());

    	// if($('#hasSize').val() == 1){

    		$('#alert-message').hide();
    		
    		function showModal(){
    			$('#exampleModal').modal('show');
    		}

    		$('#addExtraPrice').click(function(event){
    			event.preventDefault();
    			var itemId = $('#itemId').val();
    			var itemPrice = $('#extraPrice').val();
    			var extraPriceSize = $('#extraPriceSize').val();
    			var _token = '{{csrf_token()}}';

    			$.ajax({
		           type:'POST',
		           url:'{{ route('addExtraPriceItem') }}',
		           data:{item:itemId, itemSize:extraPriceSize, itemPrice:itemPrice,_token:_token},
		           success:function(data, textStatus, xhr){
		           	if(xhr.status==200){
		           		loadData();
		           		$('#exampleModal').modal('hide');
		           	}else{
		           		$('#alert-message').show();
				        // console.log(data);
				        message = jQuery.parseJSON( data.responseText )
		           		$('#alert-message').html('<strong class="text-danger">'+ message.message +'</strong>');

		           	}
		           },
		           error: function (data, textStatus, errorThrown) {
				        $('#alert-message').show();
				        // console.log(data);
				        message = jQuery.parseJSON( data.responseText );
				        if(message.msg != undefined ){
				        	$output = '<strong class="text-danger">'+ message.msg +'</strong>';
				        }else if(message.message != undefined || message.errors != undefined){
				        	$output = '<strong class="text-danger">'+ message.message + '</br>';
				        	$.each(message.errors, function(key, value){
				        		// $output += 'Error [' + key + ']' + ' : ' + value + '</br>';
				        		$output += value + '</br>';
							});
				        }
				        $output +=  '</strong>';
		           		$('#alert-message').html($output);
				   }
		        });

    		});
    	// }
	</script>
@endsection