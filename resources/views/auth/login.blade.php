@extends('admin.layouts.login')
@section('content')
<form method="POST" action="{{ route('login') }}" class="user">
    @csrf
    <div class="form-group">
        {{-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> --}}
        <input type="email" class="form-control form-control-user @error('email') is-invalid @enderror" id="email" aria-describedby="emailHelp" placeholder="Enter Email Address..." value="{{ old('email') }}" required autocomplete="email" autofocus name="email"> 
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" id="password" placeholder="Password">
        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group form-check">
        <div class="custom-control custom-checkbox small">
            <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label class="custom-control-label" for="remember">
                {{ __('Remember Me') }}
            </label>
        </div>
    </div>
    
    <button type="submit" class="btn btn-primary btn-user btn-block">
    {{ __('Login') }}
    </button>
    {{--   <a href="index.html" class="btn btn-primary btn-user btn-block">
        Login
    </a> --}}
    <hr>
    {{--  <a href="index.html" class="btn btn-google btn-user btn-block">
        <i class="fab fa-google fa-fw"></i> Login with Google
    </a>
    <a href="index.html" class="btn btn-facebook btn-user btn-block">
        <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
    </a> --}}
</form>
@if (Route::has('password.request'))
<div class="text-center">
    <a class="small" href="{{ route('password.request') }}">
        {{ __('Forgot Your Password?') }}
    </a>
</div>
@endif
@endsection