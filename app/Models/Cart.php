<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\CartItem;

class Cart extends Model
{
    protected $fillable = [];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function products(){
    	return $this->belongsToMany('App\Models\Item')->withPivot('quanity');
    }

    public function cartItems(){
        return $this->hasMany('App\Models\CartItem','cart_id');
    }

}
