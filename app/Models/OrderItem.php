<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_product';
    protected $fillable = [
						'order_id',
						'item_id',
						'quantity'
						];

    public function order(){
      $this->belongsTo(Order::class);
    }
    
    public function item(){
      $this->belongsTo(Item::class);
    } 
}
