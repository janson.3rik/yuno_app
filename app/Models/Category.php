<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use URL;
class Category extends Model
{

	public function subcategory(){
        return $this->hasMany('App\Models\Category', 'parent_id');
    }

    public function items(){
        return $this->hasMany('App\Models\Item','category_id');
    }

	public function setTitleAttribute($value)
    {
        $this->attributes['title'] = ucwords($value);
    }

    public function processImage() {
		if ($this->featuredImage) {
			return URL::asset($this->featuredImage);
		} else {
			return null;
		}
	}
}
