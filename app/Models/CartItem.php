<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $table = 'cart_items';
    protected $fillable = [
						'cart_id',
						'item_id',
            'quantity',
						'customer_id'
						];

    public function cart(){
      $this->belongsTo(Cart::class,'cart_id');
    }
    
    public function item(){
      $this->belongsTo(Item::class);
    } 
}
