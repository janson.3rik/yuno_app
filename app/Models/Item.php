<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use URL;

class Item extends Model
{
    public function processImage() {
		if ($this->featuredImage) {
			return URL::asset($this->featuredImage);
		} else {
			return null;
		}
	}

	public function itemOtherPrice(){
		return $this->hasMany('App\Models\ItemExtraPrice','itemId');
	}

	public function orderItems(){
      $this->hasMany(orderItem::class);
  	}

}
