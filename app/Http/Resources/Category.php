<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'title'=>$this->title,
            'content'=>$this->content,
            'parent_id'=>$this->parent_id,
            'featuredImage'=> $this->processImage(),
            'status'=>$this->status,
        ];
    }
}
