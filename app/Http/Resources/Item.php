<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use URL;
use App\Http\Resources\ItemExtraPrice as ItemExtraPriceResource;


class Item extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

         return [
            'data' => [

                'id' => $this->id,
                'title' => $this->title,
                'description' => $this->description,
                'category_id' => $this->category_id,
                'featured' => $this->featured,
                'featuredImage' => $this->processImage(),
                'dishType' => $this->dishType,
                'status' => $this->status,
                'hasSize' => $this->hasSize,
                'price' => $this->price,
                'price-options' => ItemExtraPriceResource::collection($this->itemOtherPrice),
                ]
            // 'links'    => [
            //     'self' => route('item.index'),
            // ],
        ];
    }

}
