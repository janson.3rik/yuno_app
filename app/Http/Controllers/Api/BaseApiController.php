<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseApiController extends Controller
{
    public function saveUpdateImage($file, $imageName, $path) {
		$imageFile = $file;
		$extension = $imageFile->getClientOriginalExtension();
		if (!empty($imageName)) {
			$imageFileName = strtolower(str_slug($imageName)) . '-image-' . strtotime(date('Y-m-d H:i:s')) . '.' . $extension;
		} else {
			$imageFileName = 'Image-' . strtotime(date('Y-m-d H:i:s')) . '.' . $extension;
		}
		$imageFileName = $path . '/' . $imageFileName;
		$imageFile->move($path, $imageFileName);
		return $imageFileName;
	}

	public function apiResponse($status,$message,$data=null,$statusCode){
		return response()->json([
                'status' => $status,
                'message' => $message,
                'data' => ($data==null)? []:$data,
            ], $statusCode);
	}
}
