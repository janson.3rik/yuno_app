<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;

class ItemController extends Controller
{
    public function getLatestItems(){
    	$data['items'] = Item::orderBy('created_at','asc')->get();
    	// dd($data['items']);
    	
		return response()->json(
		[
		    'status'=>'success',
		    'error'=>false,
		    'data'=> $data['items']
		],200);
		
    }
    public function getItemLists($id){
    	$category = Category::where([['id',$id],['status',1]])->first();


    	if($category){	
	    	$data['category'] = $category;
	    	$items =[];
	    	foreach ($category->items as $item) {
	    		$items = $item;
	    		$items->featuredImage = $item->processImage();
	    	}
	    	$data['items'] = $items;
    	}else{
	    	$data['items'] = [];
    	}
		
		return response()->json(
		[
		    'status'=>'success',
		    'error'=>false,
		    'data'=> $data['category'],
		],200);
		
    }
}
