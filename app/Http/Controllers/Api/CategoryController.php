<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use Validator;

class CategoryController extends Controller
{
    public function getCategoryLists(){
    	$data['categories'] = Category::where('status',1)->get();
    	$data['total'] = count($data['categories']);
		
		return response()->json(
		[
		    'status'=>'success',
		    'error'=>false,
		    'data'=> $data['categories']
		],200);
		
    }
    public function getCategoryItemLists(Request $request){

    	$validator = Validator::make($request->all(), [
                'category_id' => 'required|integer',
            ]);

        if($validator->fails()){
            \Log::log('info',"'Cart item delete request: ".json_encode($validator->errors()->toJson())."'");
            return response()->json(
                [
                    'status'=> 'error',
                    'error'        =>  true,
                    'error_msg'=>$validator->errors()->toJson()
                ], 400);
        }


    	$category = Category::where([['id',$request->category_id],['status',1]])->first();


    	if($category){	

    		return response()->json(
			[
				    'status'=>'success',
				    'error'=>false,
				    'data'=> $category->items()->with('itemOtherPrice')->where('status',1)->get(),
			],200);

    	}else{
	    	return response()->json(
			[
			    'status'=>'error',
			    'error'=>true,
			    'message'=> 'No Category Found.'
			],400);
    	}

		
    }
}
