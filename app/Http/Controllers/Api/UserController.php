<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\User;
use Auth;

use App\Http\Resources\UserResource;



class UserController extends BaseApiController
{
    protected $imageDestination;

    public function __construct() {
        $this->imageDestination = 'uploads/images/users';
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {

            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                'error' => true,
                'error_msg' => 'Invalid Credentials'
                ], 400);
            }

            
            $user             = User::find(Auth::user()->id);
            $userId           = $user->id;

            $data = [
                        'token' => $token,
                        'user'  => new UserResource($user),
                        'expires_in' => Auth::guard('api')->factory()->getTTL() * 60
                    ];

            return $this->apiResponse(true,"Login Successful",$data,200);

        } catch (JWTException $e) {
            return $this->apiResponse(false,"Couldn't Login.",null,400);
        }

    }

    public function getUserProfile(Request $request){
         try {

            if (! $token = JWTAuth::user()) {
                return response()->json([
                'error' => true,
                'error_msg' => 'Invalid Credentials'
                ], 400);
            }

            $user             = User::find(Auth::user()->id);
            $userId           = $user->id;
            return response([
                'status'        => 'success',
                'error'        =>  false,
                'data'          => $user,
                ], 200);


        } catch (Exception $e) {
            \Log::log('info',"'detailProfile: ".json_encode($e)."'");
        }


    }

    public function register(Request $request)
    {
        
        try {
            
            $validator = Validator::make($request->all(), [
                'username' => 'required|string|max:255',
                'first_name' => 'required|string|max:255',
                'last_name' => 'string|max:255',
                'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6|confirmed',
            ]);

             \Log::log('info',"'register: ".json_encode($request->all())."'");


            if($validator->fails()){
                \Log::log('info',"'register: ".json_encode($validator->errors()->toJson())."'");
                    return response()->json(
                        [
                            'status'=> 'error',
                            'error'        =>  true,
                            'error_msg'=>$validator->errors()->toJson()
                        ], 400);
            }



            $requestData = $request->all();


            /* Image Process */
            $image_name = '';

            // sanitize request

            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $imageName = $request->get('username');
                $path = $this->imageDestination;
                $returnedImage = $this->saveUpdateImage($file, $imageName, $path);
                $image_name = $returnedImage;
            }else {
                $image_name = null;
            }

            $user = new User();
            $user->username = $request->get('username');
            $user->first_name = ucfirst(trim($request['first_name']));
            $user->last_name = ucfirst(trim($request['last_name']));
            $user->email = $request['email'];
            $user->user_type = 'customer';
            $user->verified = 0;
            $user->password = bcrypt(trim($request->get('password')));

            $user->avatar = $image_name;

            if($user->save()){
                // $token = JWTAuth::fromUser($user);
                return response()->json(
                    [
                        'status'=>'success',
                        'error'        =>  false,
                        'message'=> 'Congratulation! You have become a member of Sewa Green Top. Please continue to login.'
                    ],200);
            }else{
                return response()->json(
                    [
                        'status'=>  'error',
                        'error'        =>  true,
                        'error_msg'=> 'Unable to confirm your registeration',
                    ], 400);
            }
        } catch (Exception $e) {
            \Log::log('info',"'registerError: ".json_encode($e)."'");
        }


    }

    public function refresh() {
        $token = Auth::guard('api')->refresh();
        if($token){

            return response([
                'status'        => 'success',
                'error'        =>  false,
                'data'          => [
                    'token' => $token,
                    'expires_in' => Auth::guard('api')->factory()->getTTL() * 60
                    ],
                ], 200);


        }else{
            return response()->json(
                    [
                        'status'=>  'error',
                        'error'        =>  true,
                        'error_msg'=> 'Unable to confirm your process.',
                    ], 400);
        }
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }

    public function logoutUser() {

        Auth::guard('api')->logout();
        if(JWTAuth::parseToken()){
            return response()->json([
                'status' => 'success',
                'error' => false,
                'message' => 'You have been logout.'
            ], 200);
        }else{
            return response()->json([
                'status' => 'Error',
                'error' => true,
                'message' => 'Please try again.'
            ], 200);
        }

        
    }

}
