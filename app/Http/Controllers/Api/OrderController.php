<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\User;
use App\Models\Item;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\OrderItem;
use Auth;

class OrderController extends BaseApiController
{

    public function addToOrder(Request $request)
    {
        try {

            if (! $token = JWTAuth::user()) {
                return response()->json([
                'error' => true,
                'error_msg' => 'Invalid Credentials'
                ], 400);
            }

            $user = JWTAuth::parseToken()->authenticate();
            $userId = $user->id;

            $validator = Validator::make($request->all(), [
                    'cart_id' => 'required|string|max:255',
                    'notes' => '',
            ]);

            if($validator->fails()){
                \Log::log('info',"'Add to Order Request: ".json_encode($validator->errors()->toJson())."'");
                return response()->json(
                    [
                        'status'=> 'error',
                        'error'        =>  true,
                        'error_msg'=>$validator->errors()->toJson()
                    ], 400);
            }


            $cart = Cart::where([
                ['id',$request->cart_id],
                ['customer_id',$userId],
                ['status',0]
            ])->orderBy('cart_created_date','desc')->first();

            if(!$cart || !count($cart->cartItems)>0){


                 \Log::log('info',"'Add to Order Request: ".json_encode($validator->errors()->toJson())."'");
                return response()->json(
                    [
                        'status'=> 'error',
                        'error'        =>  true,
                        'error_msg'=> 'Could not process your order. No items found on cart.'
                    ], 400);



            }


        } catch (Exception $e) {
            
        }
    }

    public function getListOfItemsInCart(Request $request)
    {
        try {

            if (! $token = JWTAuth::user()) {
                return response()->json([
                'error' => true,
                'error_msg' => 'Invalid Credentials'
                ], 400);
            }

            $user = JWTAuth::parseToken()->authenticate();
            $userId = $user->id;

            $carts = Cart::where('customer_id',$userId)->with('cartItems')->first();

            $data = $carts;



            if(!$carts){

                return response()->json([
                'error' => true,
                'error_msg' => 'Empty Carts.'
                ], 400);

            }

            return response()->json(
                        [
                            'status'=>'success',
                            'error'        =>  false,
                            'data'=> $data,
                        ],200);

        } catch (Exception $e) {
            \Log::log('info',"'listError: ".json_encode($e)."'");
        }    
    }

    public function getCartTotalCount(Request $request)
    {
        try {

            if (! $token = JWTAuth::user()) {
                return response()->json([
                'error' => true,
                'error_msg' => 'Invalid Credentials'
                ], 400);
            }

            $user = JWTAuth::parseToken()->authenticate();
            $userId = $user->id;

            $carts = Cart::where('customer_id',$userId)->first();

            



            if(!$carts){

                return response()->json([
                'error' => true,
                'error_msg' => 'Empty Carts.'
                ], 400);

            }

            $cartCount = count($carts->cartItems);

            $data = [
                                "total-cart-items"=>$cartCount,
                    ];

            return response()->json(
                        [
                            'status'=>'success',
                            'error'        =>  false,
                            'data'=> $data,
                        ],200);

        } catch (Exception $e) {
            \Log::log('info',"'listError: ".json_encode($e)."'");
        }    
    }

    public function addItemToCart(Request $request)
    {  
    	 
    	 try {

            if (! $token = JWTAuth::user()) {
                return response()->json([
                'error' => true,
                'error_msg' => 'Invalid Credentials'
                ], 400);
            }

            $user = JWTAuth::parseToken()->authenticate();
            $userId = $user->id;


        	$latestCart = Cart::where([
                ['customer_id',$userId],
                ['status',0]
            ])->orderBy('cart_created_date','desc')->first();


        	if($latestCart && $latestCart->status == 0){
        		$cart_id = $latestCart->id;
        	}else{
        		$newCart = new Cart();
        		$newCart->customer_id = $user->id;
        		$newCart->status = 0;
        		$newCart->cart_created_date = date('Y-m-d');
        		$newCart->cart_created_time = date('H:i:s');
        		if($newCart->save()){
                    \Log::log('info',"'cartItemAddedId: ".json_encode($newCart->id)."'"); 

        			$cart_id = $newCart->id;
        		}else{
        			 return response()->json(
    		            [
    		                'status'=>'error',
    		                'error'        =>  true,
    		                'message'=> 'Please try again',
    		            ],200);
        		}
        	}

        	$requestData = $request->all();

        	$validator = Validator::make($request->all(), [
                    'item_id' => 'required|string|max:255',
                    'quantity' => '',
            ]);


            if($validator->fails()){
                \Log::log('info',"'register: ".json_encode($validator->errors()->toJson())."'");
                return response()->json(
                    [
                        'status'=> 'error',
                        'error'        =>  true,
                        'error_msg'=>$validator->errors()->toJson()
                    ], 400);
            }
            

    		\Log::log('info',"'cartItemRequest: ".json_encode($request->all())."'"); 

    		
    		$cartItem = new CartItem();
            $item = Item::find($request->item_id);
            if(!$item){
                return response()->json(
                    [
                        'status'=>  'error',
                        'error'        =>  true,
                        'error_msg'=> 'Selected Item Could Not Found.',
                    ], 400);
            }
            $price = $item->price;
            $total_price = $request->quantity * $item->price;

            $cartItem->cart_id = $cart_id;
            $cartItem->item_id = $request->item_id;
            $cartItem->quantity = $request->quantity;
            $cartItem->name = $item->title;
            $cartItem->subtotal = $total_price;
            $cartItem->customer_id = $userId;
            $cartItem->cart_item_date = date('Y-m-d H:i:s');

    	    if($cartItem->save()){
    	                // $token = JWTAuth::fromUser($user);
    	        return response()->json(
    	            [
    	                'status'=>'success',
    	                'error'        =>  false,
    	                'message'=> 'You have place your order on your basket.Please order the basket for confirmation.Thank You.'
    	            ],200);
    	    }else{
    	        return response()->json(
    	            [
    	                'status'=>  'error',
    	                'error'        =>  true,
    	                'error_msg'=> 'Unable to process your query.',
    	            ], 400);
    	    }
        } catch (Exception $e) {
            \Log::log('info',"'registerError: ".json_encode($e)."'");
        }	
    }

    public function deleteFromCart(Request $request){

        try {

            if (! $token = JWTAuth::user()) {
                return response()->json([
                'error' => true,
                'error_msg' => 'Invalid Credentials'
                ], 400);
            }

            $user = JWTAuth::parseToken()->authenticate();
            $userId = $user->id;


            // $latestCart = Cart::where([
            //     ['customer_id',$userId],
            //     ['status',0]
            // ])->orderBy('cart_created_date','desc')->first();

            $requestData = $request->all();

            $validator = Validator::make($request->all(), [
                'cart_item_id' => 'required|integer',
            ]);

            if($validator->fails()){
                \Log::log('info',"'Cart item delete request: ".json_encode($validator->errors()->toJson())."'");
                return response()->json(
                    [
                        'status'=> 'error',
                        'error'        =>  true,
                        'error_msg'=>$validator->errors()->toJson()
                    ], 400);
            }
            

            \Log::log('info',"'cartItemRequest: ".json_encode($request->all())."'"); 


            $cartItem = CartItem::where([
                ['customer_id',$userId],
                ['id',$request->cart_item_id]
            ])->first();


            if(!$cartItem){
                return response()->json(
                        [
                            'status'=>'error',
                            'error'        =>  true,
                            'message'=> 'Item not found on your cart.',
                        ],400);

            }else{
                if($cartItem->delete()){
                    \Log::log('info',"'cartItemAddedId: ".json_encode($cartItem)."'"); 
                     return response()->json(
                        [
                            'status'=>'success',
                            'error'        =>  false,
                            'message'=> 'Item has been removed from cart.',
                        ],200);

                }else{
                     return response()->json(
                        [
                            'status'=>'error',
                            'error'        =>  true,
                            'message'=> 'Please try again',
                        ],400);
                }
            }

        } catch (Exception $e) {
            \Log::log('info',"'registerError: ".json_encode($e)."'");
        }   
    }



}
