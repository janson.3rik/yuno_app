<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use App\Http\Resources\Item as ItemResource;
use App\Http\Resources\Category as CategoryResource;

class PackageController extends Controller
{
    public function getPackageListsByCategory(Request $request){
    	$category = Category::where('id',$request->category_id)->where('status',1)->first();
    	$data['category'] = new CategoryResource($category);
    	if(!$data['category']){
    		return response()->json(
			[
			    'status'=>'error',
			    'message'=> 'Selected category not found.'
			],400);
    	}
    	$items = Item::where('category_id',$request->category_id)->where('status',1)->paginate(1);
    	$data['items'] = ItemResource::collection($items);
    	$data['total'] = count($data['items']);		
		return response()->json(
		[
		    'status'=>'success',
		    'data'=> $data
		],200);
		
    }
}
