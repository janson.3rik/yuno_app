<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class BaseController extends Controller {

	/*
	 * Assume Image parameters to defined then save the files
	 */
	public function saveUpdateImage($file, $imageName, $path) {
		$imageFile = $file;
		$extension = $imageFile->getClientOriginalExtension();
		if (!empty($imageName)) {
			$imageFileName = strtolower(str_slug($imageName)) . '-image-' . strtotime(date('Y-m-d H:i:s')) . '.' . $extension;
		} else {
			$imageFileName = 'Image-' . strtotime(date('Y-m-d H:i:s')) . '.' . $extension;
		}
		$imageFileName = $path . '/' . $imageFileName;
		$imageFile->move($path, $imageFileName);
		return $imageFileName;
	}

}
