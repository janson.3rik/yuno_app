<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Session;

class UserController extends BaseController
{
    protected $imageDestination;

    public function __construct() {
        $this->imageDestination = 'uploads/images/users';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['panel'] = 'List of Users';
        $data['rows'] = User::all();
        return view('admin.user.index',compact('data')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['panel'] = 'Add User';
        return view('admin.user.create',compact('data')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validatedData = $request->validate([
        ]);
        
        try {

            /* Image Process */
            $image_name = '';

            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $imageName = $request->get('username');
                $path = $this->imageDestination;
                $returnedImage = $this->saveUpdateImage($file, $imageName, $path);
                $image_name = $returnedImage;
            }else {
                $image_name = null;
            }

            $user = new User();
            $item->username = $request->username;
            $item->first_name = $request->firstname;
            $item->user_type = $request->usertype;
            $item->avatar = $image_name;

            if ($user->save()) {
                Session::flash('success_message', 'User has been Added.');
                return redirect('user')->with('success_message', 'User added Successfully.');
            } else {
                Session::flash('error_message', 'Item could not be add.');
                return redirect('user')->with('error_message', 'User could not be added.');
            }

        } catch (Exception $e) {
            Session::flash('error_message', 'User could not be add.');
            return redirect('user')->with('error_message', 'User could not be added.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['panel'] = 'Edit User';
        $data['row'] = User::find($id);
        return view('admin.user.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // dd($request->all());
        $validatedData = $request->validate([
          
        ]);
        
        try {

            $user = User::find($id);

            /* Image Process */
            $image_name = '';           

            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $imageName = $request->get('username');
                $path = $this->imageDestination;
                $returnedImage = $this->saveUpdateImage($file, $imageName, $path);
                $image_name = $returnedImage;
                
                if ($user->avatar) {
                    $old_img = $user->avatar;
                    if (file_exists($old_img)) {
                        unlink($old_img);
                    }
                }
            } elseif (isset($user->avatar) && !empty($user->avatar)) {
                $image_name = $user->avatar;
            }

            $user->title = $request->title;
            $user->username = $request->username;
            $user->first_name = $request->firstname;
            $user->user_type = $request->usertype;
            $user->avatar = $image_name;

            if ($user->save()) {
                Session::flash('success_message', 'User has been Updated.');
                return back();
                // return redirect('user')->with('success_message', 'user updated Successfully.');
            } else {
                Session::flash('error_message', 'User could not be updated.');
                return redirect('user')->with('error_message', 'user could not be updated.');
            }

        } catch (Exception $e) {
            Session::flash('error_message', 'User could not be updated.');
            return redirect('user')->with('error_message', 'User could not be updated.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function loadItemExtraPrice($id){
        $user = User::find($id);
        if(!$user){
            return response()->json(['msg'=>'User Not found.'],400);
        }
        if($user->delete()){
            return response()->json(['msg'=>'User Deleted.'],200);
        }
      }

}
