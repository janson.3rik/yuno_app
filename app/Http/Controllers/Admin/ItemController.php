<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\ItemExtraPrice;
use App\Models\Category;
use Validator;
use Session;

class ItemController extends BaseController
{
    protected $imageDestination;

    public function __construct() {
        $this->imageDestination = 'uploads/images/items';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['panel'] = 'List of Items';
        $data['rows'] = Item::all();
        return view('admin.item.index',compact('data')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['panel'] = 'Add Item';
        $data['categories'] = json_encode(Category::where('status',1)->select('id','title')->pluck('title','id')->toArray());
        return view('admin.item.create',compact('data')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'title' => 'required',
            'description' => 'min:6',
            'category_id' => 'required',
            'featured'=>'required|in:0,1',
            'file'=>'',
            'dishType'=>'required|string:NotApplicable,Veg,NonVeg',
            'status'=>'required|in:0,1',
            'hasSize'=>'required|in:0,1',
            'price'=>'',
        ]);
        
        try {

            /* Image Process */
            $image_name = '';

            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $imageName = $request->get('title');
                $path = $this->imageDestination;
                $returnedImage = $this->saveUpdateImage($file, $imageName, $path);
                $image_name = $returnedImage;
            }else {
                $image_name = null;
            }

            $item = new Item();
            $item->title = $request->title;
            $item->category_id = ($request->category_id == '' || $request->category_id == null )? 0:$request->category_id;
            $item->status = $request->status;
            $item->description = $request->description;
            $item->featured = $request->featured;
            $item->dishType = $request->dishType;
            $item->hasSize = $request->hasSize;
            if(!$request->hasSize){   
                $item->price = $request->price;
            }
            $item->featuredImage = $image_name;

            if ($item->save()) {
                Session::flash('success_message', 'Item has been Added.');
                return redirect('item')->with('success_message', 'Item added Successfully.');
            } else {
                Session::flash('error_message', 'Item could not be add.');
                return redirect('item')->with('error_message', 'Item could not be added.');
            }

        } catch (Exception $e) {
            Session::flash('error_message', 'Item could not be add.');
            return redirect('item')->with('error_message', 'Item could not be added.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['panel'] = 'Edit Item';
        $data['row'] = Item::find($id);
        $data['categories'] = json_encode(Category::where('status',1)->select('id','title')->pluck('title','id')->toArray());
        return view('admin.item.edit',compact('data'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // dd($request->all());
        $validatedData = $request->validate([
            'title' => 'required',
            'description' => 'min:6',
            'category_id' => 'required',
            'featured'=>'required|in:0,1',
            'file'=>'',
            'dishType'=>'required|string:NotApplicable,Veg,NonVeg',
            'status'=>'required|in:0,1',
            'hasSize'=>'required|in:0,1',
            'price'=>'',
        ]);
        
        try {

            $item = Item::find($id);

            /* Image Process */
            $image_name = '';           

            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $imageName = $request->get('title');
                $path = $this->imageDestination;
                $returnedImage = $this->saveUpdateImage($file, $imageName, $path);
                $image_name = $returnedImage;
                
                if ($item->featuredImage) {
                    $old_img = $item->featuredImage;
                    if (file_exists($old_img)) {
                        unlink($old_img);
                    }
                }
            } elseif (isset($item->featuredImage) && !empty($item->featuredImage)) {
                $image_name = $item->featuredImage;
            }

            $item->title = $request->title;
            $item->category_id = ($request->category_id == '' || $request->category_id == null )? 0:$request->category_id;
            $item->status = $request->status;
            $item->description = $request->description;
            $item->featured = $request->featured;
            $item->dishType = $request->dishType;
            $item->hasSize = $request->hasSize;
            if($request->hasSize == 0){   
                $item->price = $request->price;
            }else{
                $item->price = null;
            }

            $item->featuredImage = $image_name;

            if ($item->save()) {
                Session::flash('success_message', 'Item has been Updated.');
                return back();
                // return redirect('item')->with('success_message', 'Item updated Successfully.');
            } else {
                Session::flash('error_message', 'Item could not be updated.');
                return redirect('item')->with('error_message', 'Item could not be updated.');
            }

        } catch (Exception $e) {
            Session::flash('error_message', 'Item could not be updated.');
            return redirect('item')->with('error_message', 'Item could not be updated.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function loadItemExtraPrice($id){
        $item = Item::find($id);
        if(!$item){
            return response()->json(['msg'=>'The Extra Price for Item has been added.'],400);
        }else{
            $items = ItemExtraPrice::where('itemId',$id)->get();
            if(count($items)>0){
                $output = "";
                foreach ($items as $row) {
                    $output .= '<tr> <td>'. $row->size . '</td> <td>' .$row->price . '</td></tr>';
                }
                return $output;
            }else{
                return response()->json(['msg'=>'No Item Found.'],400);
            }

        }
      }

    public function addExtraPriceItem(Request $request){
        try {
            $item = Item::find($request->item);
            if(!$item){
                return response()->json(['msg'=>'The Item Could Not be found.'],400);
            }
            $validatedData = $request->validate([
                'itemSize' => 'required',
                'itemPrice' => 'required',
            ]);
            $itemExtraPrice = new ItemExtraPrice();
            $itemExtraPrice->itemId = $item->id;
            $itemExtraPrice->size = $request->itemSize;
            $itemExtraPrice->price = $request->itemPrice;
            if($itemExtraPrice->save()){
                return response()->json(['msg'=>'The Extra Price for Item has been added.'],200);
            }else{
                return response()->json(['msg'=>'The Extra Price for Item has been added.'],400);
            }
        } catch (Exception $e) {
            dd($e->getMessage());   
        }
    }
}
