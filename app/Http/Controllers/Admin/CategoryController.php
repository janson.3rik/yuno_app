<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Validator;
use Session;

class CategoryController extends BaseController
{
    protected $imageDestination;

    public function __construct() {
        $this->imageDestination = 'uploads/images/category';
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['panel'] = 'List of Categories';
        $data['rows'] = Category::where('status',1)->get();
        return view('admin.category.index',compact('data')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['panel'] = 'Add Category';
        $data['categories'] = json_encode(Category::where('status',1)->select('id','title')->pluck('title','id')->toArray());
        return view('admin.category.create',compact('data')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'parentCategory' => '',
            'categoryTitle' => 'required',
            'file' => '',
            'description' => 'min:6',
            'status' => 'required|in:0,1',
        ]);

        try {
            // dd($request->all());

            /* Image Process */
            $image_name = '';

            // sanitize request

            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $imageName = $request->get('categoryTitle');
                $path = $this->imageDestination;
                $returnedImage = $this->saveUpdateImage($file, $imageName, $path);
                $image_name = $returnedImage;
            }else {
                $image_name = null;
            }

            $category = new Category();
            $category->title = $request->categoryTitle;
            $category->parent_id = ($request->parentCategory == '' || $request->parentCategory == null )? 0:$request->parentCategory;
            $category->status = $request->status;
            $category->content = $request->description;
            $category->featuredImage = $image_name;

            if ($category->save()) {
                Session::flash('success_message', 'Category has been Added.');
                return redirect('category')->with('success_message', 'Category added Successfully.');
            } else {
                Session::flash('error_message', 'Category could not be add.');
                return redirect('category')->with('error_message', 'Category could not be added.');
            }

        } catch (Exception $e) {
            Session::flash('error_message', 'Category could not be add.');
            return redirect('category')->with('error_message', 'Category could not be added.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['panel'] = 'Edit Category';
        $data['row'] = Category::find($id);
        $data['categories'] = json_encode(Category::where('status',1)->select('id','title')->pluck('title','id')->toArray());
        return view('admin.category.edit',compact('data')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        
        
        $validatedData = $request->validate([
            'parentCategory' => '',
            'categoryTitle' => 'required',
            'file' => '',
            'description' => 'min:6',
            'status' => 'required|in:0,1',
        ]);

        try {

            $category = Category::find($id);

            /* Image Process */
            $image_name = '';

            // sanitize request

            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $imageName = $request->get('categoryTitle');
                $path = $this->imageDestination;
                $returnedImage = $this->saveUpdateImage($file, $imageName, $path);
                $image_name = $returnedImage;
                
                if ($category->featuredImage) {
                    $old_img = $category->featuredImage;
                    if (file_exists($old_img)) {
                        unlink($old_img);
                    }
                }
            } elseif (isset($category->featuredImage) && !empty($category->featuredImage)) {
                $image_name = $category->featuredImage;
            }

            $category->title = $request->categoryTitle;
            $category->parent_id = ($request->parentCategory == '' || $request->parentCategory == null )? 0:$request->parentCategory;
            $category->status = $request->status;
            $category->content = $request->description;
            $category->featuredImage = $image_name;

            if ($category->save()) {
                Session::flash('success_message', 'Category has been Added.');
                return redirect('category')->with('success_message', 'Category added Successfully.');
            } else {
                Session::flash('error_message', 'Category could not be add.');
                return redirect('category')->with('error_message', 'Category could not be added.');
            }

        } catch (Exception $e) {
            Session::flash('error_message', 'Category could not be add.');
            return redirect('category')->with('error_message', 'Category could not be added.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
