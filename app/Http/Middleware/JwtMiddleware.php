<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        \Log::log('info',"'detailProfile: ".json_encode(getallheaders())."'");

        try {
            $user = JWTAuth::parseToken()->authenticate();
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['status' => 'error','message'=>'user_not_found'], 404);
            }
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json([
                    'status' => false,
                    'message'=>'Token is Invalid',
                    'data'=> [],
                ],401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json([
                    'status' => false,
                    'message'=>'Token is Expired',
                    'data'=> [],
                    ],
                    440);
            }else{
                return response()->json([
                    'status' => false,
                    'message'=>'Authorization Token not found',
                    'data'=> [],
                ],401);
            }
        }
        return $next($request);
        





        // try {
        //     $user = JWTAuth::parseToken()->authenticate();
        //     if (!$user = JWTAuth::parseToken()->authenticate()) {
        //             return response()->json(['status' => 'error','message'=>'user_not_found'], 404);
        //     }
        // } catch (Exception $e) {
        //     if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
        //         return response()->json([
        //             'status' => 'error',
        //             'error'=>true,
        //             'message'=>'Token is Invalid'
        //         ],401);
        //     }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
        //         return response()->json([
        //             'status' => 'error','message'=>'Token is Expired'],
        //             440);
        //     }else{
        //         return response()->json(['status' => 'error','message'=> 'Authorization Token not found'],401);
        //     }
        // }
        // return $next($request);


    }
}
