<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
	        'username' => 'admin',
	        'first_name' => 'admin',
	        'user_type' => 'super-admin',
	        'verified' => 1,
	        'email' => 'iron@gmail.com',
	        'email_verified_at' => now(),
	        'password' => bcrypt('hello123'), // password
	        'remember_token' => Str::random(10),
	    ]);
    }
}
