<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->unsigned()->nullable();
            
            $table->foreign('customer_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('set null');

            $table->string('customer_name')->nullable();
            $table->string('number')->nullable();
            $table->string('paid_type')->nullable();
            $table->tinyInteger('status')->unsigned()->default(0)->nullable();
            $table->string('order_date')->nullable();
            $table->string('order_time')->nullable();
            $table->string('tax')->nullable();
            $table->string('discount')->nullable();
            $table->string('total')->nullable();
            $table->string('total_items')->nullable();
            $table->string('paid_method')->nullable();
            $table->string('tax_amount')->nullable();
            $table->string('discount_amount')->nullable();
            $table->string('coupon')->nullable();
            $table->string('order_entered_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
