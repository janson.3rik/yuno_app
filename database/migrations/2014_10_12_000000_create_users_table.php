<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            
            $table->string('avatar')->nullable();
            $table->string( 'user_type' )->default('customer');
            
            $table->text('like_items')->nullable();
            $table->text('dislike_items')->nullable();
            $table->text('favourite_items')->nullable();
            $table->text('favourite_categories')->nullable();
            $table->text('preferences')->nullable();
            
            $table->boolean('active')->default(0)->nullable();
            $table->boolean('verified')->default(1);
            $table->string('last_login_date')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
