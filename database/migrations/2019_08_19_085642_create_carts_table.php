<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->unsigned()->nullable();
            $table->foreign('customer_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('set null');
            $table->string('number')->nullable();
            $table->tinyInteger('status')->unsigned()->default(0)->nullable();
            $table->string('cart_created_date')->nullable();
            $table->string('cart_created_time')->nullable();
            $table->string('total')->nullable();
            $table->string('total_items')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
